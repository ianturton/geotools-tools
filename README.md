# Geotools-Tools


## Description

This is a simple example of how to add a tool to the GeoTools swing map display environment, it was driven by 
my attempt to answer this [gis.stackexchange.com question](https://gis.stackexchange.com/q/458918/79) about 
how to draw a line on top of a map. It seems that there isn't much about how to do this in the [GeoTools 
documentation](https://docs.geotools.org/stable/userguide/unsupported/swing/index.html). Maybe in the future 
I'll rework this to be a full tutorial but not at present.


## Installation

Installation is a simple case of cloning this repository and typing `mvn install` you can then add the 
resultant jar file into your application. 

There is a small demo program that can be run using `mvn compile exec:java` which will pop up a window with a 
simple map of the USA. The only difference between this and the usual GeoTools quickstart is that an 
additional button is available in the toolbar, a pencil icon, which allows you to draw lines by clicking on 
the map. A double click finishes the line and allows you to start another one.


## Usage

If you plan on using this in a real project then you should only need to add the jar file to your project and 
then use code like:

~~~java
mapFrame = new JMapFrame();
mapFrame.enableToolBar(true);
JToolBar toolBar = mapFrame.getToolBar();
JButton btn = new JButton(new DigitizerAction(mapFrame.getMapPane()));
toolBar.addSeparator();
toolBar.add(btn);
~~~

## Support

I'm not planning on supporting this project, but if you find a bug then by all means raise an issue and 
produce a pull request if possible.

## Authors and acknowledgment

Ian Turton, 

## License

This project is licensed under the LGPL2.1.

