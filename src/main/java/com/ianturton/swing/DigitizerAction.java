package com.ianturton.swing;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import org.geotools.swing.MapPane;
import org.geotools.swing.action.MapAction;
import org.opengis.feature.simple.SimpleFeature;

public class DigitizerAction extends MapAction {
    public DigitizerAction(MapPane mapPane){
        this(mapPane, false);
    }
    public DigitizerAction(MapPane mapPane, boolean showName){
        String toolName = showName ? Digitizer.TOOL_NAME : null;
        super.init(mapPane, toolName, Digitizer.TOOL_TIP, Digitizer.ICON_IMAGE);
    }

    Digitizer digitizer =new Digitizer();
    public void actionPerformed(ActionEvent ev){getMapPane().setCursorTool(digitizer);}

    List<SimpleFeature> getFeatures(){
        return digitizer.getFeatures();
    }
}
