package com.ianturton.swing;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JToolBar;
import org.geotools.data.FileDataStore;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.feature.SchemaException;
import org.geotools.map.FeatureLayer;
import org.geotools.map.MapContent;
import org.geotools.styling.SLD;
import org.geotools.swing.JMapFrame;
import org.geotools.TestData;
import org.geotools.util.URLs;

public class DrawLineDemo {
    static JMapFrame mapFrame;
    static MapContent map;

    public static void main(String[] args) throws SchemaException, IOException {
        mapFrame = new JMapFrame();
        mapFrame.enableToolBar(true);
        JToolBar toolBar = mapFrame.getToolBar();
        JButton btn = new JButton(new DigitizerAction(mapFrame.getMapPane()));
        toolBar.addSeparator();
        toolBar.add(btn);


        map = new MapContent();
        map.setTitle("Quickstart");

        FileDataStore dataStore = new ShapefileDataStore(TestData.url("shapes/statepop.shp"));
        FeatureLayer layer = new FeatureLayer(dataStore.getFeatureSource(),
                SLD.createLineStyle(Color.blue, 2.0f));
        // Now display the map
        map.addLayer(layer);
        mapFrame.setMapContent(map);
        mapFrame.setSize(600, 600);
        mapFrame.setVisible(true);

    }

}

