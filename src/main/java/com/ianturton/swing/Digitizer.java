package com.ianturton.swing;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import org.geotools.data.DataUtilities;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.map.FeatureLayer;
import org.geotools.styling.SLD;
import org.geotools.styling.Style;
import org.geotools.swing.event.MapMouseEvent;
import org.geotools.swing.tool.CursorTool;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

public class Digitizer extends CursorTool {
    public static final String ICON_IMAGE = "/com/ianturton/swing/images/pencil.png";
    public static final String TOOL_NAME = "";
    public static final String TOOL_TIP = "Click to add point to line, double click to end";
    SimpleFeatureBuilder featureBuilder;
    GeometryFactory geometryFactory;
    ArrayList<Coordinate> positions = new ArrayList<>();

    private Cursor cursor;

    List<SimpleFeature> features = new ArrayList<>();
    Style style;
    FeatureLayer layer;
    int lastX, lastY;


    boolean first = true;

    public Digitizer() {
        SimpleFeatureTypeBuilder b = new SimpleFeatureTypeBuilder();
        b.setName("LineFeature");
        b.add("line", LineString.class);
        SimpleFeatureType TYPE = b.buildFeatureType();
        featureBuilder = new SimpleFeatureBuilder(TYPE);
        geometryFactory = JTSFactoryFinder.getGeometryFactory(JTSFactoryFinder.EMPTY_HINTS);
        style = SLD.createLineStyle(Color.red, 2.0f);

        ImageIcon imgIcon = new ImageIcon(getClass().getResource(ICON_IMAGE));
        cursor = new Cursor(Cursor.CROSSHAIR_CURSOR);
    }

    public List<SimpleFeature> getFeatures() {
        return features;
    }

    private void drawTheLine(ArrayList<Coordinate> coordinates) {

        LineString lineString = geometryFactory.createLineString(
                (Coordinate[]) coordinates.toArray(new Coordinate[0]));
        featureBuilder.add(lineString);
        SimpleFeature feature = featureBuilder.buildFeature(null);
        features.add(feature);
        getMapPane().getMapContent().removeLayer(layer);
        layer = new FeatureLayer(DataUtilities.collection(features), style);
        getMapPane().getMapContent().addLayer(layer);

        positions = new ArrayList<>();
    }

    @Override
    public void onMouseClicked(MapMouseEvent e) {

        if (e.getClickCount() > 1) { // was it a double click
            drawTheLine(positions);
            first = true;
        } else { // add a new point
            DirectPosition2D pos = e.getWorldPos();

            positions.add(new Coordinate(pos.x, pos.y));
            // Put a marker at each digitized point
            Graphics graphics = (Graphics2D) ((JComponent) getMapPane()).getGraphics().create();
            int x = e.getX();
            int y = e.getY();
            if (!first) {
                graphics.drawLine(lastX, lastY, x, y);
            }
            first = false;
            lastX = x;
            lastY = y;
            graphics.fillRect(x - 3, y - 3, 6, 6);
        }
    }

    @Override
    public Cursor getCursor() {
        return cursor;
    }
}
